export function notify(message, type = 'danger') {
  const el = $(
    `<div class="alert alert-${type} c2c-alert" role="alert">${message}</div>`
  )
  $('.d-notifications').append(el)
  setTimeout(() => el.fadeOut(() => el.remove()), 3000)
}
