import {context, descartesMap, initializeMap} from './app'

// generic expand/collapse
Array.from(document.body.querySelectorAll('.toggle')).forEach(el => {
  const target = document.body.querySelector(el.getAttribute('data-target'))
  el.addEventListener('click', () => {
    target.classList.toggle('show')
    updateToggles()
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'))
    }, 500)
  })
})
const updateToggles = () => {
  Array.from(document.body.querySelectorAll('.toggle')).forEach(el => {
    const target = document.body.querySelector(el.getAttribute('data-target'))
    const active = target.classList.contains('show')
    const openedLabel = el.getAttribute('data-label-opened')
    const closedLabel = el.getAttribute('data-label-closed')
    const label = active ? openedLabel : closedLabel
    const labelEl = el.querySelector(el.getAttribute('data-label-target'))
    if (labelEl) {
      if (label) labelEl.innerHTML = label
    } else {
      if (label) el.innerHTML = label
    }
    // The code could be simpler using `classList.toggle` with 2 args but
    // this breaks IE11 compatibility, see: https://caniuse.com/#feat=classlist
    const toggleClass = el.getAttribute('data-class-toggle')
    if (toggleClass) {
      if (active) el.classList.add(toggleClass)
      else el.classList.remove(toggleClass)
    }
  })
}

// map tools tabs
const mapToolTabs = Array.from(
  document.body.querySelectorAll('.map-tools-tabs > button')
)
const mapToolPanel = document.body.querySelector('.map-tools')
mapToolTabs.forEach(el => {
  const toggleTarget = document.body.querySelector(
    el.getAttribute('data-target')
  )
  el.addEventListener('click', () => {
    mapToolTabs.forEach(el => {
      const target = document.body.querySelector(el.getAttribute('data-target'))
      if (target === toggleTarget) {
        const toolPanelVisible = mapToolPanel.classList.contains('show')
        const targetVisible = target.classList.contains('show')
        const active = !toolPanelVisible || !targetVisible
        if (active) {
          target.classList.add('show')
          mapToolPanel.classList.add('show')
          el.classList.add('active')
        } else {
          target.classList.remove('show')
          mapToolPanel.classList.remove('show')
          el.classList.remove('active')
        }
      } else {
        target.classList.remove('show')
        el.classList.remove('active')
      }
      updateToggles()
    })
  })
})

export function setMapTitle(title) {
  Array.from(document.body.querySelectorAll('.map-title')).forEach(el => {
    el.textContent = title
  })
  document.title = `${title}`
}

export function setVersionLabel(projectVersion, commitHash, commitMessage) {
  document.body.querySelectorAll('.version-label').forEach(el => {
    el.textContent = `Version ${projectVersion}`
    el.title = `Version : ${projectVersion}
Commit : ${commitHash}
Message : ${commitMessage}`
  })
}

document.body.querySelector("#ExporterContexte").onclick = function() {
	 if (context.features.requestManager) {
         delete context.features.requestManager.options.olMap;
     }
     if (context.features.actions) {
	     let locAdressItem = context.features.actions.find(item => item.div === "localisationAdresse");
	     locAdressItem.type = "Descartes.Action.LocalisationAdresse";
         let printItem = context.features.actions.find(item => item.div === "Print");
         printItem.type = "Descartes.Action.InkMapParamsManager";
     }
	 context.mapContent.items = descartesMap.mapContent.serialize();
	 let size = descartesMap.OL_map.getSize();
	 context.map.mapParams.initExtent = descartesMap.OL_map.getView().calculateExtent(size);
	 context.infosSiteOrig= {
        locationHref: document.location.href.split('#')[0],
        action: "d-demomap-export",
        date: Descartes.Utils.formatCurrentDate()
     };
	 Descartes.Utils.download("mapContext.descartes", JSON.stringify(context));
}
document.body.querySelector("#ImporterContexte").onclick = function() { 
	 window.location.href="?contentType=contextImported";
}

export function openImportModal(descartes) {
	 let content = ` 
	 	<div class="form-group"> 
			<div class="form-group"> 
    			<label class="DescartesUILabel">Sélectionner un fichier : </label></br>
    			<input type="file" name="fileImportContexteCarte" id="fileImportContexteCarte">
			</div>
		</div>
	 `;
	 
	 let dialog = new Descartes.UI.ModalFormDialog({
	    id: 'import_dialog',
	    title: "import",
	    formClass: 'form-horizontal',
	    sendLabel: "Importer",
	    size: 'modal-sm',
	    content: content
	 });
	  let selectedFile;
       
	 dialog.open(
		function () {
			let file = document.getElementById("fileImportContexteCarte").files[0];
			let reader = new FileReader();

        	reader.addEventListener("loadend", () => {
	              let json = JSON.parse(reader.result);
	              if (json.features.actions) {
	     			let locAdressItem = json.features.actions.find(item => item.div === "localisationAdresse");
	     			if (locAdressItem) {
	     				locAdressItem.type = Descartes.Action.LocalisationAdresse;
	     		    }
         			let printItem = json.features.actions.find(item => item.div === "Print");
         			if (printItem) {
         			    printItem.type = Descartes.Action.InkMapParamsManager;
         			}
     			  }
     			  if (json.features.openlayersFeatures && json.features.openlayersFeatures.interactions) {
					let dragPanItem = json.features.openlayersFeatures.interactions.find(item => item.type === "DragPan");
					if (dragPanItem && dragPanItem.args && dragPanItem.args.kinetic) {
         			    delete dragPanItem.args.kinetic;
         			}
                  }
		          let dMap = Descartes.applyDescartesContext(json);
		          /*document.querySelectorAll(".DescartesToolBar").forEach(el => {
					  el.remove();
				  });
				  document.querySelectorAll(".map-tools-zoom").forEach(el => {
					  el.remove();
				  });*/
		          
		          initializeMap(descartes,null,dMap);
		          
		          if (document.body.querySelector('#Legend').className === 'show') {
			        dMap.OL_map.getControls().forEach(control => {
					  if (control && control.CLASS_NAME === 'Descartes.Info.Legend') {
						 dMap.OL_map.removeEventListener('moveend', control.update)
						 dMap.OL_map.removeControl(control)
					  }
			        })
				    var dLegend = dMap.addInfo({
			           type: descartes.Map.LEGEND_INFO,
			           div: 'Legend',
			           options: {
			             displayLayerTitle: true,
			          }
			        }) 
			        dLegend.update()	
				  }

				}
			); 
			
			reader.readAsText(file);
	    },
	    function () {
		  window.location.href="?contentType=d-geoserver";
		}
	 );
}

