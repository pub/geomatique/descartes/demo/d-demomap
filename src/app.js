import {getInitContextGeoserver} from './initcontext_geoserver'
import {getInitContextMapserver} from './initcontext_mapserver'
import {getInitContextQgisserver} from './initcontext_qgisserver'
import {getInitContextCarto2} from './initcontext_carto2'
import {getInitContextWfs} from './initcontext_wfs'
import {getInitContextKml} from './initcontext_kml'
import {getInitContextCluster} from './initcontext_cluster'
import {getInitContextGeoJson} from './initcontext_geojson'
import {getInitContextEditionWfs} from './initcontext_editionwfs'
import {getInitContextEditionKml} from './initcontext_editionkml'
import {getInitContextEditionGeoJson} from './initcontext_editiongeojson'
import {getInitContextVide} from './initcontext_vide'
import {getInitContextGeoplateforme} from './initcontext_geoplateforme'
import {getInitContextVectortile} from './initcontext_vectortile'
import {
  setMapTitle,
  setVersionLabel,
  openImportModal
} from './layout'
import {
  getDContext,
} from './d-api'
import SETTINGS from './settings'
import { readSearchParams } from './params.utils'

export let context;
export let descartesMap;

export async function initialize(descartes) {
	
  const {
	descartesUrlRoot,
    projectVersion,
    commitHash,
    commitMessage
  } = SETTINGS

  // 0. adjust layout for non map-related info
  setVersionLabel(projectVersion, commitHash, commitMessage)

  // 1. load url params and adapt layout with displayElementLayoutParams
  const { additionalParams, displayElementLayoutParams } = readSearchParams()

  if (!displayElementLayoutParams.displayHeader) {
    document.body.querySelector('#frontoffice-header').className = ''
    document.body.querySelector('#frontoffice-header').style.display = 'none'
    document.body.querySelector('#frontoffice-header-btn').className = ''
    document.body.querySelector('#frontoffice-header-btn').style.display = 'none'
  }

  if (!displayElementLayoutParams.displayFooter) {
    document.body.querySelector('#frontoffice-footer').className = ''
    document.body.querySelector('#frontoffice-footer').style.display = 'none'
  }

  if (!displayElementLayoutParams.displayTopbar) {
    document.body.querySelector('#frontoffice-topbar').className = ''
    document.body.querySelector('#frontoffice-topbar').style.display = 'none'
  }

  if (!displayElementLayoutParams.displayAssistants) {
    document.body.querySelector('#frontoffice-assistants-nav-item').className = ''
    document.body.querySelector('#frontoffice-assistants-nav-item').style.display = 'none'
    document.body.querySelector('#frontoffice-assistants-map-tools').className = ''
    document.body.querySelector('#frontoffice-assistants-map-tools').style.display = 'none'
  }
  
  if (!displayElementLayoutParams.displayTools) {
    document.body.querySelector('#frontoffice-tools').className = ''
    document.body.querySelector('#frontoffice-tools').style.display = 'none'
  }
  
  // 2a. InitMap
  context = getInitContextMapserver(descartesUrlRoot);
  setMapTitle("D-DemoMap - Application de démonstration de Descartes")


  // 2c. adapt Descartes context with share context
  let dcontext = null
  let cimported = false;
  if (additionalParams.dcontext !== undefined && additionalParams.dcontext !== null && additionalParams.dcontext !== "") {
	dcontext = await getDContext(additionalParams.dcontext)
	if (dcontext !== undefined && dcontext !== null && dcontext !== {}) {
	   if (dcontext.items) {
          context.mapContent.items = dcontext.items;
       }
       if (dcontext.bbox) {
          context.map.mapParams.initExtent = [dcontext.bbox.xMin, dcontext.bbox.yMin, dcontext.bbox.xMax, dcontext.bbox.yMax];
       }
    }
  } else if (additionalParams.contentType !== undefined && additionalParams.contentType !== null && additionalParams.contentType !== "") {
		if (additionalParams.contentType === "d-geoserver") {
			context = getInitContextGeoserver(descartesUrlRoot);
		} else if (additionalParams.contentType === "d-mapserver") {
			context = getInitContextMapserver(descartesUrlRoot);
		} else if (additionalParams.contentType === "d-qgisserver") {
			context = getInitContextQgisserver(descartesUrlRoot);
		} else if (additionalParams.contentType === "geoide-carto2") {
			context = getInitContextCarto2();
		} else if (additionalParams.contentType === "geoplateforme") {
			context = getInitContextGeoplateforme();
		} else if (additionalParams.contentType === "vectortile") {
			context = getInitContextVectortile();
		} else if (additionalParams.contentType === "vide") {
			context = getInitContextVide();
		} else if (additionalParams.contentType === "geojson") {
			context = getInitContextGeoJson();
		} else if (additionalParams.contentType === "wfs") {
			context = getInitContextWfs(descartesUrlRoot);
		} else if (additionalParams.contentType === "kml") {
			context = getInitContextKml();
		} else if (additionalParams.contentType === "cluster") {
			context = getInitContextCluster();
		} else if (additionalParams.contentType === "editiongeojson") {
			context = getInitContextEditionGeoJson(descartesUrlRoot);
		} else if (additionalParams.contentType === "editionwfs") {
			context = getInitContextEditionWfs(descartesUrlRoot);
		} else if (additionalParams.contentType === "editionkml") {
			context = getInitContextEditionKml(descartesUrlRoot);
		} else if (additionalParams.contentType === "contextImported") {
			cimported = true;
			openImportModal(descartes);
		}
  }
  
  if (!cimported) {
  
	initializeMap(descartes,dcontext,null,additionalParams.contentType);

  }

}

export function initializeMap(descartes,dcontext,dMap, contentType) {
	  
	  if (contentType !== "editiongeojson" && contentType !== "editionkml" && contentType !== "editionwfs") {
		  // 2d. add annotations layer
		  if (context.mapContent.items) {
			var isPresent = false;
			context.mapContent.items.forEach(function (item) {
		       if (item.options && item.options.id === "dAnnotationsLayer") {
		          isPresent = true;
		          return;
		       }
		    }, this);
		    if (!isPresent) {
			   var coucheAnnotations = {
				  id: 1,
			      title: "Ma couche Annotations",
			      type: descartes.Layer.EditionLayer.TYPE_Annotations,
			      itemType:"Layer"
			   };
		       context.mapContent.items.unshift(coucheAnnotations);
		    }
		  }
		  
		  if (contentType === "vectortile") {
			//switch open panel
			document.body.querySelector('#Legend').className = "";
			document.body.querySelector('#legendBtn').className = "";
			document.body.querySelector('#mapContentBtn').className = "active";
			document.body.querySelector('#mapContent').className = "show";
		  }  
	  } else {
		//switch open panel
		document.body.querySelector('#Legend').className = "";
		document.body.querySelector('#legendBtn').className = "";
		document.body.querySelector('#mapContentBtn').className = "active";
		document.body.querySelector('#mapContent').className = "show";
	  }
	
	  // 3. apply Descartes context
	  if (dMap) {
		descartesMap = dMap;
	  } else {
	  	descartesMap = descartes.applyDescartesContext(context)
	  }
	
	  // 4. adapt zoom with share context
	  if (dcontext !== undefined && dcontext !== null && dcontext !== {}) {   
	        descartesMap.OL_map.getView().setZoom(dcontext.zoom);
	  }
	
	  // 5. Add legend: independent management
	  const legend = document.body.querySelector('.map-tools-tabs > #legendBtn')
	  legend.addEventListener('click', () => {
		  if (document.body.querySelector('#Legend').className === 'show') {
	        descartesMap.OL_map.getControls().forEach(control => {
			  if (control.CLASS_NAME === 'Descartes.Info.Legend') {
				 descartesMap.OL_map.removeEventListener('moveend', control.update)
				 descartesMap.OL_map.removeControl(control)
			  }
	        })
		    var dLegend = descartesMap.addInfo({
	           type: descartes.Map.LEGEND_INFO,
	           div: 'Legend',
	           options: {
	             displayLayerTitle: true,
	          }
	        }) 
	        dLegend.update()	
		  } else {
	        descartesMap.OL_map.getControls().forEach(control => {
			  if (control.CLASS_NAME === 'Descartes.Info.Legend') {
				 descartesMap.OL_map.removeEventListener('moveend', control.update)
				 descartesMap.OL_map.removeControl(control)
			  }
	        })
		  }
	  })
	  const mapToolTabs = Array.from(
	     document.body.querySelectorAll('.map-tools-tabs > button')
	  )
	  mapToolTabs.forEach(el => {
	     el.addEventListener('click', () => {
	        if (el.getAttribute('data-target') !== "#Legend") {
	          descartesMap.OL_map.getControls().forEach(control => {
			     if (control && control.CLASS_NAME === 'Descartes.Info.Legend') {
	                descartesMap.OL_map.removeEventListener('moveend', control.update)
				    descartesMap.OL_map.removeControl(control)
			     }
	          })
	        }
	     })
	  })
}