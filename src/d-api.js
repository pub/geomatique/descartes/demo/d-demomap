import { notify } from './notifications'

export function getDContext(contextFile) {
   return fetch(Descartes.CONTEXT_MANAGER_SERVER+"?" + contextFile)
    .then(resp => {
      if (!resp.ok) throw new Error()
      return resp.json()
    })
    .catch(() =>
      notify(`Le chargement du fichier de context partagé a échoué.`)
    )
}
