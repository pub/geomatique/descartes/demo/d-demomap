export const DISPLAY_PROJECTIONS = [
  // Systèmes de coordonnées géographiques
  'EPSG:4326',
  'EPSG:4258',
  'EPSG:4171',
  // Systèmes de projection
  // Monde
  'EPSG:3857',
  'EPSG:3395',
  // France Métropolitaine
  'EPSG:2154',
  'EPSG:3942',
  'EPSG:3943',
  'EPSG:3944',
  'EPSG:3945',
  'EPSG:3946',
  'EPSG:3947',
  'EPSG:3948',
  'EPSG:3949',
  'EPSG:3950',
  'EPSG:32630',
  'EPSG:32631',
  'EPSG:32632',
  'EPSG:27572',
  // DOM TOM
  'EPSG:4559',
  'EPSG:5490',
  'EPSG:2972',
  'EPSG:2975',
  'EPSG:4471',
  'EPSG:4467',
  'EPSG:3296',
  'EPSG:3297',
  'EPSG:3298',
  'EPSG:3299',
]
