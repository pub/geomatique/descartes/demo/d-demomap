export function readSearchParams() {
  var urlParams = new URLSearchParams(window.location.search)
  const additionalParams = readAdditionalParams(urlParams)
  const displayElementLayoutParams = readDisplayElementLayoutParams(urlParams)
  return { additionalParams, displayElementLayoutParams }
}

function readAdditionalParams(urlParams) {
  let dcontext = getContextSearchParam(urlParams)
  let contentType = getContentTypeParam(urlParams)
  return { dcontext, contentType }
}

function getContextSearchParam(urlParams) {
   const contextFile = urlParams.get('context')
   return contextFile
}

function getContentTypeParam(urlParams) {
   const contentType = urlParams.get('contentType')
   return contentType
}

function readDisplayElementLayoutParams(urlParams) {
  let displayHeader, displayTopbar, displayFooter, displayAssistants, displayTools
  displayHeader = true
  if (urlParams.get('displayHeader') === "false") {
    displayHeader = false	
  }
  displayTopbar = true
  if (urlParams.get('displayTopbar') === "false") {
    displayTopbar = false	
  }
  displayFooter = true
  if (urlParams.get('displayFooter') === "false") {
    displayFooter = false	
  }
  displayAssistants = true
  if (urlParams.get('displayAssistants') === "false") {
    displayAssistants = false	
  }
  displayTools = true
  if (urlParams.get('displayTools') === "false") {
    displayTools = false	
  }
  return { displayHeader, displayTopbar, displayFooter, displayAssistants, displayTools }
}