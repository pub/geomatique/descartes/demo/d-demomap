class Settings {
  constructor() {
	this.descartesApiPath = ''
	this.descartesUrlRoot = ''
	this.georefApiPath = ''
    this.projectVersion = ''
    this.commitHash = ''
    this.commitMessage = ''
  }

  init() {
    return fetch('settings.json')
      .then(response => response.json())
      .then(json => {
        Object.assign(this, json)
      })
  }
}

const SETTINGS = new Settings()
export default SETTINGS
