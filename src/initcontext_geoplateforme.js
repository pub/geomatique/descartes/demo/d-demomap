export function getInitContextGeoplateforme() {
  return {
    "map": {
        "type": "Discrete",
        "div": "map",
        "mapParams": {
            "maxExtent": [
                -20037508,
                -20037508,
                20037508,
                20037508
            ],
            "autoSize": true,
            "projection": "EPSG:3857",
            "initExtent": [
                -800086,
                5055726,
                1150172,
                6772047
            ],
            "minScale": null,
            "maxScale": null,
            "resolutions": [
                /*156543.03,
                104362.02,
                69574.68000000001,
                46383.12,
                30922.08,
                20614.72,
                13743.146666666667,
                9162.097777777779,
                6108.065185185186,*/
                4072.0434567901243,
                2714.695637860083,
                1809.797091906722,
                1206.5313946044814,
                804.3542630696543,
                536.2361753797695,
                357.49078358651303,
                238.32718905767535,
                158.8847927051169,
                105.9231951367446,
                70.6154634244964,
                47.076975616330934,
                31.38465041088729,
                20.92310027392486,
                13.948733515949906,
                9.299155677299938,
                6.199437118199959,
                4.132958078799972,
                2.7553053858666483,
                1.8368702572444322,
                1.224580171496288,
                0.816386780997525,
                0.544257853998350,
                0.362838569332233,
                0.2418923795548223,
                0.1612615863698815,
                0.1075077242465877,
                0.0716718161643918,
                0.0477812107762612,
                0.0318541405175074,
                0.0212360936783383,
                0.0141573957855588,
                0.0094382638570392,
                0.0062921759046928,
                0.0041947839364618,
                0.0027965226243079
            ]
        }
    },
    "mapContent": {
        "mapContentParams": {
            "editable": true,
            "editInitialItems": true, 
            "fctOpacity": true,
            "fctDisplayLegend": true,
            //"fctMetadataLink": true,
            "displayMoreInfos": true,
            "displayIconLoading": true
        },
        "mapContentManager": {
            "div": "layersTree",
            "contentTools": [
    			{"type" : Descartes.Action.MapContentManager.ADD_GROUP_TOOL},
    			{"type" : Descartes.Action.MapContentManager.ADD_LAYER_TOOL},
    			{"type" : Descartes.Action.MapContentManager.REMOVE_GROUP_TOOL},
    			{"type" : Descartes.Action.MapContentManager.REMOVE_LAYER_TOOL},
    			{"type" : Descartes.Action.MapContentManager.ALTER_GROUP_TOOL},
    			{"type" : Descartes.Action.MapContentManager.ALTER_LAYER_TOOL},
    			{"type" : Descartes.Action.MapContentManager.ADD_WMS_LAYERS_TOOL}
            ],
            "options": {
                "toolBarDiv": "managerToolBar",
                "uiOptions": {
					"nodeRootTitle": "Contenu de la carte",
                    "moreInfosUiParams": {
                        "fctOpacity": false,
                        "fctDisplayLegend": true,
                        "displayOnClickLayerName": true
                    },
		            "resultUiParams": {
		              "withReturn": true,
		              "withUIExports": true,
		              "withAvancedView": true,
		              "withListResultLayer": true,
		              "withFilterColumns": true,
		              "withResultLayerExport": true,
		            }
                }
            }
        },
        "items": [
            {
	            "id": 300569261,
	            "itemType": "Layer",
	            "title": "ADMINEXPRESS COG CARTO 2023 (WFS)",
	            "type": 4,
	            "options": {
	                "id": "300569261",
	                "visible": false,
	                "opacity": 100,
	                "maxScale": null,
	                "minScale": null,
	                "queryable": false
	            },
	            "definition": [
	                {
	                    "serverUrl": "https://data.geopf.fr/wfs/ows",
	                    "layerName": "ADMINEXPRESS-COG-CARTO.LATEST:departement",
	                    "serverVersion": "2.0.0",
	                    "featureServerUrl": "https://data.geopf.fr/wfs/ows",
	                    "featureServerVersion": "2.0.0",
	                    "featureGeometryName": "geom",
	                    "useBboxSrsProjection": true
	                }
	            ]
            },{
                "id": 520181085,
                "itemType": "Layer",
                "title": "Plan cadastral informatisé vecteur de la DGFIP (WMS)",
                "type": 0,
                "options": {
                    "id": "520181085",
                    "visible": true,
                    "opacity": 100,
                    "minScale": null,
                    "maxScale": null,
                    "legend": [
                        "https://data.geopf.fr/annexes/ressources/legendes/CADASTRALPARCELS.PARCELLAIRE_EXPRESS.png"
                    ],
                    "attribution": "©IGN",
                    "geometryType": "Polygon",
                    "queryable": false
                },
                "definition": [
                    {
                        "serverUrl": "https://data.geopf.fr/wms-r?",
                        "serverVersion": "1.3.0",
                        "layerName": "CADASTRALPARCELS.PARCELLAIRE_EXPRESS",
                        "featureGeometryName": "geom"
                    }
                ]
            },{
                "id": 520181084,
                "itemType": "Layer",
                "title": "Forêts publiques (WMS)",
                "type": 0,
                "options": {
                    "id": "520181084",
                    "visible": true,
                    "opacity": 100,
                    "minScale": null,
                    "maxScale": null,
                    "legend": [
                        "https://data.geopf.fr/annexes/ressources/legendes/onf_forets_publiques.png"
                    ],
                    "attribution": "©IGN",
                    "geometryType": "Polygon",
                    "queryable": false
                },
                "definition": [
                    {
                        "serverUrl": "https://data.geopf.fr/wms-r?",
                        "serverVersion": "1.3.0",
                        "layerName": "FORETS.PUBLIQUES",
                        "featureGeometryName": "geom"
                    }
                ]
            },{
                "id": 520181083,
                "itemType": "Layer",
                "title": "Courbes de niveau",
                "type": 0,
                "options": {
                    "id": "520181083",
                    "visible": true,
                    "opacity": 100,
                    "minScale": null,
                    "maxScale": null,
                    "legend": [
                    ],
                    "attribution": "©IGN",
                    "geometryType": "Polygon",
                    "queryable": false
                },
                "definition": [
                    {
                        "serverUrl": "https://data.geopf.fr/wms-r?",
                        "serverVersion": "1.3.0",
                        "layerName": "ELEVATION.CONTOUR.LINE",
                        "featureGeometryName": "geom"
                    }
                ]
            },
            {
                "id": 520181082,
                "itemType": "Layer",
                "title": "Limites administratives Express COG (WMS)",
                "type": 0,
                "options": {
                    "id": "520181082",
                    "visible": true,
                    "opacity": 100,
                    "minScale": null,
                    "maxScale": null,
                    "legend": [
                    ],
                    "attribution": "©IGN",
                    "geometryType": "Point",
                    "queryable": false
                },
                "definition": [
                    {
                        "serverUrl": "https://data.geopf.fr/wms-r?",
                        "serverVersion": "1.3.0",
                        "layerName": "ADMINEXPRESS-COG-CARTO.LATEST"
                    }
                ]
            },
            {
				"itemType" : "Group",
				"title" : "Fonds de carte",
				"options" : {
					"opened" : false,
					"addedByUser" : false,
					"visible" : null
				},
				"items" : [
					{
			            "id": 800569256,
			            "itemType": "Layer",
			            "title": "Cartes IGN (WMTS private)",
			            "type": 3,
			            "options": {
			                "id": "800569256",
			                "visible": false,
			                "opacity": 100,
			                "minScale": null,
			                "maxScale": null,
			                "matrixSet": "PM",
			                "projection": "EPSG:3857",
			                "matrixIds": ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"],
						    "origins": [[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248]],
				            "resolutions": [156543.033928041,78271.5169640205,39135.7584820102,19567.8792410051,9783.93962050256,4891.96981025128,2445.98490512564,1222.99245256282,611.49622628141,305.748113140705,152.874056570353,76.4370282851762,38.2185141425881,19.1092570712941,9.55462853564703,4.77731426782352,2.38865713391176,1.19432856695588,0.597164283477939],
				            "tileSizes": [256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256],
			                "format": "image/jpeg",
			                "queryable": false
			            },
			            "definition": [
			                {
			                    "serverUrl": "https://data.geopf.fr/private/wmts?apikey=iqqdkoa235g0mf1gr3si0162",
			                    "serverVersion": "1.0.0",
			                    "layerName": "GEOGRAPHICALGRIDSYSTEMS.MAPS",
			                    "extent": [-20037508.342789244,-10488397.72802615,20037508.342789244,15538711.096309226],
			                    "layerStyles": "normal"
			                }
			            ]
			        },
			        {
			            "id": 800569255,
			            "itemType": "Layer",
			            "title": "SCAN25TOUR WLD WM 12-2022 (WMS-R private)",
			            "type": 0,
			            "options": {
			                "id": "800569255",
			                "visible": false,
			                "opacity": 100,
			                "minScale": null,
			                "maxScale": null,
			                "legend": [
			                    "/rest-api/v1/proxy?https://data.geopf.fr/private/wms-r?apikey=iqqdkoa235g0mf1gr3si0162&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=SCAN25TOUR_PYR-JPEG_WLD_WM&LEGEND_OPTIONS=forceLabels%3Aon"
			                ],
			                "queryable": true
			            },
			            "definition": [
			                {
			                    "serverUrl": "https://data.geopf.fr/private/wms-r?apikey=iqqdkoa235g0mf1gr3si0162",
			                    "serverVersion": "1.3.0",
			                    "layerName": "SCAN25TOUR_PYR-JPEG_WLD_WM",
			                    "featureGeometryName": null,
			                    "internalProjection": "EPSG:3857",
			                    "useBboxSrsProjection": false
			                }
			            ]
			        },
			        {
			            "id": 800569253,
			            "itemType": "Layer",
			            "title": "BDParcellaire (WMS-R)",
			            "type": 0,
			            "options": {
			                "id": "800569253",
			                "visible": false,
			                "opacity": 100,
			                "minScale": null,
			                "maxScale": null,
			                "legend": [
			                    "/rest-api/v1/proxy?https://data.geopf.fr/wms-r?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetLegendGraphic&FORMAT=image%2Fpng&LAYER=CP.CadastralParcel&LEGEND_OPTIONS=forceLabels%3Aon"
			                ],
			                "queryable": false
			            },
			            "definition": [
			                {
			                    "serverUrl": "https://data.geopf.fr/wms-r",
			                    "serverVersion": "1.3.0",
			                    "layerName": "CP.CadastralParcel",
			                    "featureGeometryName": null,
			                    "internalProjection": "EPSG:3857",
			                    "useBboxSrsProjection": false
			                }
			            ]
			        },{
			                "id": 520181081,
			                "itemType": "Layer",
			                "title": "Plan IGN v2 (WMS)",
			                "type": 0,
			                "options": {
			                    "id": "520181081",
			                    "visible": false,
			                    "opacity": 100,
			                    "minScale": null,
			                    "maxScale": null,
			                    "legend": [
			                        "https://data.geopf.fr/annexes/ressources/legendes/GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2-legend.png"
			                    ],
			                    "attribution": "©IGN",
			                    "geometryType": "Point",
			                    "queryable": false
			                },
			                "definition": [
			                    {
			                        "serverUrl": "https://data.geopf.fr/wms-r?",
			                        "serverVersion": "1.3.0",
			                        "layerName": "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2"
			                    }
			                ]
			            },        
			            {
				            "id": 800569257,
				            "itemType": "Layer",
				            "title": "Plan IGN v2 (WMTS)",
				            "type": 3,
				            "options": {
				                "id": "800569257",
				                "visible": false,
				                "opacity": 100,
				                "minScale": null,
				                "maxScale": null,
				                "matrixSet": "PM",
				                "projection": "EPSG:3857",
				                "matrixIds": ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19"],
								"origins": [[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248]],
				                "resolutions": [156543.033928041,78271.5169640205,39135.7584820102,19567.8792410051,9783.93962050256,4891.96981025128,2445.98490512564,1222.99245256282,611.49622628141,305.748113140705,152.874056570353,76.4370282851762,38.2185141425881,19.1092570712941,9.55462853564703,4.77731426782352,2.38865713391176,1.19432856695588,0.597164283477939,0.29858214173897],
				                "tileSizes": [256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256],
				                "format": "image/png",
				                "queryable": false
				            },
				            "definition": [
				                {
				                    "serverUrl": "https://data.geopf.fr/wmts?SERVICE=WMTS",
				                    "serverVersion": "1.0.0",
				                    "layerName": "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
				                    "extent": [
				                        -19480910.888822876,
				                        -19971868.880408563,
				                        19480910.888822876,
				                        19971868.88040853
				                    ],
				                    "layerStyles": "normal"
				                }
				            ]
				        },{
						    "id": 520181100,
						    "itemType": "Layer",
						    "title": "ORTHOPHOTOS - Photographies aériennes IGN (WMTS)",
						    "type": 3,
						    "options": {
						        "id": "520181100",
						        "visible": true,
						        "opacity": 50,
						        "minScale": null,
						        "maxScale": null,
						        "matrixSet": "PM",
						        "projection": "EPSG:3857",
						        "matrixIds": ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21"],
						        "origins": [[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248],[-20037508.342789248, 20037508.342789248]],
						        "resolutions": [156543.033928041,78271.51696402048,39135.758482010235,19567.87924100512,9783.93962050256,4891.96981025128,2445.98490512564,1222.99245256282,611.49622628141,305.7481131407048,152.8740565703525,76.43702828517624,38.21851414258813,19.10925707129406,9.554628535647032,4.777314267823516,2.388657133911758,1.194328566955879,0.5971642834779395,0.2985821417389697,0.1492910708694849,0.0746455354347424],
						        "tileSizes": [256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256],
						        "format": "image/jpeg",
                    			"attribution": "©IGN",
						        "queryable": false
						    },
						    "definition": [
						        {
						            "serverUrl": "https://data.geopf.fr/wmts?",
						            "serverVersion": "1.0.0",
						            "layerName": "ORTHOIMAGERY.ORTHOPHOTOS",
						            "extent": [-19981848.597392607,-12932243.11199203,19981848.597392607,12932243.11199202],
						            "layerStyles": "normal"
						        }
						    ]
						}
				]
			}
        ]
    },
    "features": {
        "toolBars": [
            {
                "div": "toolbar-root",
                "tools": [
                    {
                        "type": "ZoomToInitialExtent",
                        "args": [
                            333837,
                            5315435,
                            1014141,
                            5592847
                        ]
                    }
                ],
                "options": {
                    "vertical": true
                }
            },
            {
                "div": "toolbar-layerstreesimple",
                "tools": [
                    {
                        "type": "DisplayLayersTreeSimple"
                    }
                ],
                "options": {
                    "vertical": true
                }
            },
            {
                "div": "toolbar-measure",
                "tools": [
                    {
                        "type": "MeasureDistance"
                    },
                    {
                        "type": "MeasureArea"
                    }
                ]
            },
            {
                "div": "toolbar-query",
                "tools": [
                    {
                        "type": "PointSelection",
                        "args": {
                            "resultLayerParams": {
                                "display": true
                            },
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            }
                        }
                    },
                    {
                        "type": "PolygonSelection",
                        "args": {
                            "resultLayerParams": {
                                "display": true
                            },
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            }
                        }
                    },                   {
                        "type": "CircleSelection",
                        "args": {
                            "resultLayerParams": {
                                "display": true
                            },
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            }
                        }
                            },
                    {
                        "type": "RectangleSelection",
                        "args": {
                            "persist": true,
                            "resultLayerParams": {
                                "display": true
                            },
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            }
                        }
                    },
                    {
                        "type": "PointRadiusSelection",
                        "args": {
                            "persist": true,
                            "infoRadius": true,
                            "exportBuffer": true,
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            },
                            "resultLayerParams": {
                                "display": true
                            }
                        }
                    },
                    {
                        "type": "LineBufferHaloSelection",
                        "args": {
                            "persist": true,
                            "infoBuffer": true,
                            "configHalo": false,
                            "exportBuffer": true,
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            },
                            "resultLayerParams": {
                                "display": true
                            }
                        }
                    },
                    {
                        "type": "PolygonBufferHaloSelection",
                        "args": {
                            "persist": true,
                            "infoBuffer": true,
                            "configHalo": false,
                            "exportBuffer": true,
                            "resultUiParams": {
                                "withReturn": true,
                                "withUIExports": true,
                                "withAvancedView": true,
                                "withResultLayerExport": true,
                                "withFilterColumns": true,
                                "withListResultLayer": true
                            },
                            "resultLayerParams": {
                                "display": true
                            }
                        }
                    }
                ]
            },
            {
                "div": "toolbar-export",
                "tools": [
                    {
                        "type": "ExportPDF",
                        "args": {
                            "infos": {
                                "title": "carte_descartes_exemples"
                            }
                        }
                    },
                    {
                        "type": "ExportPNG",
                        "args": {
                            "infos": {
                                "title": "carte_descartes_exemples"
                            }
                        }
                    }
                ]
            },
            {
                "div": "toolbar-sharelink",
                "tools": [
                    {
                        "type": "ShareLinkMap"
                    }
                ],
                "options": {
                    "vertical": true
                }
            },
            {
                "div": "toolbar-geolocations",
                "tools": [
                    {
                        "type": "GeolocationSimple",
                        "args": {
                            "displayProjections": ["EPSG:4326"]
                        }
                    },
                    {
                        "type": "GeolocationTracking"
                    }
                ],
                "options": {
                    "enableHighAccuracy": true
                }
            },
            {
                "div": "toolbar-geoplateforme",
                "tools": [
                    {
                        "type": Descartes.Button.GeoPlateformeRoute
                    },
                    {
                        "type": Descartes.Button.GeoPlateformeIsoCurve
                    },
                    {
                        "type": Descartes.Button.GeoPlateformeGetAltitude
                    }
                ],
                "options": {}
            }
        ],
        "annotationToolBars": [
            {
                "div": "toolbar-annotations",
                "tools": [
                    {
                        "type": "ToolBarOpener",
                        "args": {
                            "displayClass": "editionOpenerButton",
                            "title": "Outils d'annotations",
                            "tools": [
                                {
                                    "type": "EditionAideAnnotation"
                                },
                                {
                                    "type": "EditionDrawAnnotation",
                                    "args": {
                                        "geometryType": "Point",
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionDrawAnnotation",
                                    "args": {
                                        "geometryType": "Line",
                                        "snapping": true,
                                        "autotracing": true
                                    }
                                },
                                {
                                    "type": "EditionDrawAnnotation",
                                    "args": {
                                        "geometryType": "Polygon",
                                        "snapping": true,
                                        "autotracing": true
                                    }
                                },
                                {
                                    "type": "EditionDrawAnnotation",
                                    "args": {
                                        "geometryType": "Rectangle",
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionDrawAnnotation",
                                    "args": {
                                        "geometryType": "Circle",
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionFreehandAnnotation",
                                    "args": {
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionTextAnnotation",
                                    "args": {
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionBufferHaloAnnotation"
                                },
                                {
                                    "type": "EditionGlobalModificationAnnotation"
                                },
                                {
                                    "type": "EditionVerticeModificationAnnotation",
                                    "args": {
                                        "snapping": true
                                    }
                                },
                                {
                                    "type": "EditionAddTextAnnotation"
                                },
                                {
                                    "type": "EditionAttributeAnnotation"
                                },
                                {
                                    "type": "EditionStyleAnnotation"
                                },
                                {
                                    "type": "EditionSnappingAnnotation"
                                },
                                {
                                    "type": "EditionRubberAnnotation"
                                },
                                {
                                    "type": "EditionEraseAnnotation"
                                },
                                {
                                    "type": "EditionExportAnnotation"
                                },
                                {
                                    "type": "EditionImportAnnotation"
                                }
                            ],
                            "toolBarOptions": {
                                "panel": true,
                                "draggable": {
                                    "enable": true,
                                    "containment": "parent"
                                },
                                "resizable": true
                            }
                        }
                    }
                ],
                "options": {}
            },
        ],
        "infos": [
            {
				"type" : "Attribution", 
				"div" : null
			},
            {
                "type": "MetricScale",
                "div": "NumericScale"
            },
            {
                "type": "GraphicScale",
                "div": "ScaleBar"
            },
            {
                "type": "Legend",
                "div": "Legend",
                "options": {
                    "displayLayerTitle": true
                }
            },
            {
                "type": "LocalizedMousePosition",
                "div": "LocalizedMousePosition",
                "options": {
                    "projection": true,
                    "numDigits": 6,
                    "displayProjections": [
                        "EPSG:4326",
                        "EPSG:4326-DMS",
                        "EPSG:4326-DM",
                        "EPSG:4258",
                        "EPSG:4258-DMS",
                        "EPSG:4258-DM",
                        "EPSG:4171",
                        "EPSG:4171-DMS",
                        "EPSG:4171-DM",
                        "EPSG:3857",
                        "EPSG:3395",
                        "EPSG:2154",
                        "EPSG:3942",
                        "EPSG:3943",
                        "EPSG:3944",
                        "EPSG:3945",
                        "EPSG:3946",
                        "EPSG:3947",
                        "EPSG:3948",
                        "EPSG:3949",
                        "EPSG:3950",
                        "EPSG:32630",
                        "EPSG:32631",
                        "EPSG:32632",
                        "EPSG:27572",
                        "EPSG:4559",
                        "EPSG:5490",
                        "EPSG:2972",
                        "EPSG:2975",
                        "EPSG:4471",
                        "EPSG:4467",
                        "EPSG:3296",
                        "EPSG:3297",
                        "EPSG:3298",
                        "EPSG:3299"
                    ],
                    "selectedDisplayProjectionIndex": 9
                }
            }
        ],
        "actions": [
            {
                "type": "ScaleSelector",
                "div": "ScaleSelector",
                "options": {
                    "label": true
                }
            },
            {
                "type": "ScaleChooser",
                "div": "ScaleChooser",
                "options": {
                    "size": 5,
                    "label": true
                }
            },
            {
                "type": "CoordinatesInput",
                "div": "CoordinatesInput",
                "options": {
                    "projection": true,
                    "checkCoordinatesInMaxExtent": false,
                    "displayProjections": [
                        "EPSG:4326",
                        "EPSG:4258",
                        "EPSG:4171",
                        "EPSG:3857",
                        "EPSG:3395",
                        "EPSG:2154",
                        "EPSG:3942",
                        "EPSG:3943",
                        "EPSG:3944",
                        "EPSG:3945",
                        "EPSG:3946",
                        "EPSG:3947",
                        "EPSG:3948",
                        "EPSG:3949",
                        "EPSG:3950",
                        "EPSG:32630",
                        "EPSG:32631",
                        "EPSG:32632",
                        "EPSG:27572",
                        "EPSG:4559",
                        "EPSG:5490",
                        "EPSG:2972",
                        "EPSG:2975",
                        "EPSG:4471",
                        "EPSG:4467",
                        "EPSG:3296",
                        "EPSG:3297",
                        "EPSG:3298",
                        "EPSG:3299"
                    ],
                    "selectedDisplayProjectionIndex": 3
                }
            },
            /*{
                "type": "SizeSelector",
                "div": "SizeSelector",
                "options": {
                    "sizeList": [
                        [
                            450,
                            300
                        ],
                        [
                            600,
                            400
                        ],
                        [
                            750,
                            500
                        ],
                        [
                            900,
                            600
                        ]
                    ],
                    "defaultSize": 1,
                    "label": true
                }
            },*/
            {
				"div": "localisationAdresse",
				"type": Descartes.Action.LocalisationAdresse
            },
            /*{
                "div": "Print",
                "options": {
                    "label": true,
                    "infos": {
                        "title": "carte_descartes_exemples",
                        "producer": "Service producteur : Formations géomatiques",
                        "attributions": "Données © MTES",
                        "logoUrl": "bloc-marque_MTES.png",
                        "defaultFormatCode": "A4L"
                    },
                    "params": {
                        "displayProjections": [
                            "EPSG:4326",
                            "EPSG:4258",
                            "EPSG:4171",
                            "EPSG:3857",
                            "EPSG:3395",
                            "EPSG:2154",
                            "EPSG:3942",
                            "EPSG:3943",
                            "EPSG:3944",
                            "EPSG:3945",
                            "EPSG:3946",
                            "EPSG:3947",
                            "EPSG:3948",
                            "EPSG:3949",
                            "EPSG:3950",
                            "EPSG:32630",
                            "EPSG:32631",
                            "EPSG:32632",
                            "EPSG:27572",
                            "EPSG:4559",
                            "EPSG:5490",
                            "EPSG:2972",
                            "EPSG:2975",
                            "EPSG:4471",
                            "EPSG:4467",
                            "EPSG:3296",
                            "EPSG:3297",
                            "EPSG:3298",
                            "EPSG:3299"
                        ],
                        "selectedDisplayProjectionIndex": 3
                    }
                }
            }*/
            {
	          type: Descartes.Action.InkMapParamsManager,
	          div: 'Print',
	          options: {
	            label: true,
	            infos: {
	              title: "D-Demomap - Application de démonstration de Descartes",
	              producer: 'Service producteur : XXX',
	              attributions: 'Données © MTES',
	              logoUrl: 'bloc-marque_MTES.png',
	              defaultFormatCode: 'A4L',
	            },
	            params: {
	              displayProjections: [
                            "EPSG:4326",
                            "EPSG:4258",
                            "EPSG:4171",
                            "EPSG:3857",
                            "EPSG:3395",
                            "EPSG:2154",
                            "EPSG:3942",
                            "EPSG:3943",
                            "EPSG:3944",
                            "EPSG:3945",
                            "EPSG:3946",
                            "EPSG:3947",
                            "EPSG:3948",
                            "EPSG:3949",
                            "EPSG:3950",
                            "EPSG:32630",
                            "EPSG:32631",
                            "EPSG:32632",
                            "EPSG:27572",
                            "EPSG:4559",
                            "EPSG:5490",
                            "EPSG:2972",
                            "EPSG:2975",
                            "EPSG:4471",
                            "EPSG:4467",
                            "EPSG:3296",
                            "EPSG:3297",
                            "EPSG:3298",
                            "EPSG:3299",
                           ],
	              selectedDisplayProjectionIndex: 3,
	            },
	          },
	         }
        ],
        /*"directionalPanPanel": {
            "options": {}
        },*/
        /*"miniMap": {
            "resourceUrl": "http://mapserveur.application.developpement-durable.gouv.fr/map/mapserv?map=/opt/data/carto/cartelie/prod/PNE_IG/OSM_2.i2.map&LAYERS=c_natural_Valeurs_type",
            "options": {}
        },*/
        /*"toolTip": {
            "div": "ToolTip",
            "toolTipLayers": [
                {
                    "layerId": "820181080",
                    "fields": [
                        "name"
                    ]
                },
                {
                    "layerId": "820181081",
                    "fields": [
                        "name"
                    ]
                }
            ],
            "options": {
                "delay": 500,
                "displayDelay": 5000,
                "displayOnClick": true
            }
        },*/
        /*"bookmarksManager": {
            "div": "Bookmarks",
            "mapName": "exemple-descartes",
            "options": {}
        },*/
        "gazetteer": {
            "div": "Gazetteer",
            "initValue": "",
            "levels": [
                {
                    "message": "Choisissez une région",
                    "error": "Aucune région",
                    "name": "reg",
                    "options": {}
                },
                {
                    "message": "Choisissez un département",
                    "error": "Aucun département",
                    "name": "dept",
                    "options": {}
                },
                {
                    "message": "Choisissez une commune",
                    "error": "Aucune commune",
                    "name": "com",
                    "options": {}
                },
                {
                    "message": "Choisissez une section cadastrale",
                    "error": "Aucune section",
                    "name": "",
                    "options": {}
                },
                {
                    "message": "Choisissez une parcelle cadastrale",
                    "error": "Aucune parcelle",
                    "name": "",
                    "options": {}
                }
            ],
            "options": {
                "service": {
					"type": "localisationParcellaire",
					"niveauBase": 0
                }
            }
        },
        /*"requestManager": {
            "div": "Requetes",
            "requests": [
                {
                    "layerId": "820181080",
                    "title": "Filtrage des objets de la couche \"Stations essence\"",
                    "geometryType": "Point",
                    "requestMembers": [
                        {
                            "title": "name",
                            "field": "name",
                            "operator": "~",
                            "value": [],
                            "visible": true
                        }
                    ],
                    "options": {}
                },
                {
                    "layerId": "820181081",
                    "title": "Filtrage des objets de la couche \"Parkings\"",
                    "geometryType": "Point",
                    "requestMembers": [
                        {
                            "title": "name",
                            "field": "name",
                            "operator": "~",
                            "value": [],
                            "visible": true
                        }
                    ],
                    "options": {}
                }
            ],
            "options": {
	            "withChooseExtent": true,
                "resultUiParams": {
                    "withReturn": true,
                    "withUIExports": true,
                    "withAvancedView": true,
                    "withResultLayerExport": true,
                    "withFilterColumns": true,
                    "withListResultLayer": true
                }
            }
        },*/
        "openlayersFeatures": {
            "controls": [
                {
                    "type": Descartes.Map.OL_ZOOM,
                    "args": {
                        "target": "map-zoom",
                        "className": "map-tools-zoom"
                    }
                },
                {
                    "type": Descartes.Map.OL_ROTATE,
                    "args": {
                        "target": "map-rotate",
                        "className": "map-tools-rotate"
                    }
                }
            ],
            "interactions": [
                {
                    "type": Descartes.Map.OL_MOUSE_WHEEL_ZOOM
                },
                {
                    "type": Descartes.Map.OL_DRAG_PAN,
                    "args": {
                        condition:ol.events.condition.noModifierKeys/*,
                        kinetic: new ol.Kinetic(-0.01, 0.1, 200)*/
                    }
                },
                {
                    "type": Descartes.Map.OL_DRAG_ZOOM
                },
                {
                    "type": Descartes.Map.OL_DRAG_ROTATE
                },
                {
                    "type": Descartes.Map.OL_DRAG_ROTATE_AND_ZOOM
                },
                {
                    "type": Descartes.Map.OL_PINCH_ZOOM
                }
            ]
        }
    }
};
}