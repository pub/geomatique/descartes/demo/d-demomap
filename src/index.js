import { initialize as initApp } from './app'
import './layout'
import SETTINGS from './settings'

import './scss/main.scss'

SETTINGS.init()
  .then(() => {
    const {
      descartesApiPath,
      georefApiPath,
    } = SETTINGS

    if (`${descartesApiPath}`.includes("preprod.descartes-server")) {
		Descartes.setWebServiceInstance('preprod');
    }

    Descartes.PROXY_SERVER = `${descartesApiPath}/proxy?`
    Descartes.EXPORT_PNG_SERVER = `${descartesApiPath}/exportPNG`
    Descartes.EXPORT_PDF_SERVER = `${descartesApiPath}/exportPDF`
    Descartes.EXPORT_CSV_SERVER = `${descartesApiPath}/csvExport`
    Descartes.FEATURE_SERVER = `${descartesApiPath}/getFeature`
    Descartes.FEATUREINFO_SERVER = `${descartesApiPath}/getFeatureInfo`
    Descartes.CONTEXT_MANAGER_SERVER = `${descartesApiPath}/contextManager`
    
    Descartes.GEOREF_SERVER = `${georefApiPath}`
    
    Descartes.Descartes_Papers = [
      { name: 'A0', width: 841, length: 1189 },
      { name: 'A1', width: 594, length: 841 },
      { name: 'A2', width: 420, length: 594 },
      { name: 'A3', width: 297, length: 420 },
      { name: 'A4', width: 210, length: 297 },
      { name: 'A5', width: 148, length: 210 },
    ]
    Descartes.Layer.ResultLayer.config.displayMarker = true
    Descartes.EditionManager.configure({
        globalEditionMode: false
    });
    
    Descartes.Messages.Descartes_Messages_UI_GazetteerInPlace.TITLE_MESSAGE="Localisation parcellaire";
    
    return initApp(window.Descartes)
  })
