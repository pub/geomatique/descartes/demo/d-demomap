module.exports = {
  transformIgnorePatterns: [],
  transform: {
    '\\.xml$': 'jest-raw-loader',
    '^.+\\.[jt]sx?$': 'babel-jest',
  },
}
