# D-DemoMap

L'application D-DemoMap est une application de démonstration qui utilise les librairies javascript (paquets NPM) D-Map, D-EditMap et D-InkMap ainsi que les services web du module D-WsMap de Descartes

## Accès à l'application de démonstration

Instance de Prod:

```bash
https://descartes.din.developpement-durable.gouv.fr/demo/
```

Instance de PreProd:

```bash
https://preprod.descartes.din.developpement-durable.gouv.fr/demo/
```

## Utilisation du composant en cours de développement

### Pre-requis

Installation sur son poste:
* NODE 16.14.0
* NPM 8.5.2

### Installation

L'installation se fait via la commande `npm install`. 
Remarque: les paquets NPM de Descartes sont associées au scope **@descartes** et sont accessibles dans un repository NPM Public dans Gitlab.

Configuration du fichier `.npmrc` vers l'entrepot Public:

```shell
@descartes:registry=https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/projects/17127/packages/npm/
```

### Construction

L'application peut être construite avec:

```bash
npm run build
```

Le livrable se trouve dans le répertoire `dist`. Le répertoire `public` contient les fichiers statics qui font partis de l'application.


### Lancement

```bash
npm run start
```

L'application est lancée sur http://localhost:4200.

A noter que l'application a besoin que les services web de D-WsMap soit lancés et accessibles sur http://localhost:8080.

## Configuration

Les paramètres de configuration de l'application se trouvent dans le fichier `conf/settings.json`. Certains de ces paramètres ont des valeurs dynamiques (git hash, project version, ...), et d'autres sont positionnés au lancement de l'application.

## Déploiement dans un serveur web

Pour déployer l'application sur un server web comme Nginx ou Apache, il suffit d'exposer un répertoire contenant le contenu des deux  répertoire `dist` et `public`.

Pour NGINX par exemple, ces fichiers doivent être positionnés dans le répertoire `/usr/share/nginx/html` sans configuration supplémentaire.

## Utilisation de l'image DOCKER

L'image Docker n'est pas disponible sur les dépôts public (ex: hub.docker.com).
Elle est disponible sur le dépôt de container GitLab MTE suivant:  

```bash
registry.gitlab-forge.din.developpement-durable.gouv.fr/pub/geomatique/descartes/demo/d-demomap/docker-d-demomap:x.x.x
```
