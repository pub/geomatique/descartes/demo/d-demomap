const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const dMapVersion = require('@descartes/d-map/package.json').version
const dEditMapVersion = require('@descartes/d-editmap/package.json').version
const dInkmapVersion = require('@descartes/d-inkmap/package.json').version
const dGeoplateformeVersion = require('@descartes/d-geoplateforme/package.json').version

const descartesApiPath = require('./conf/settings.json').descartesApiPath.replace('/v1','')

var descartesServer = {
    protocol: 'http',
    host: 'localhost',
    port: 8080,
    path: '/d-wsmap/api/v1/'
};

module.exports = {
  mode: 'development',
  entry: {
    polyfills: path.resolve(__dirname, 'src', 'polyfills.js'),
    index: [
      '@babel/polyfill/noConflict',
      path.resolve(__dirname, 'src', 'index.js'),
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].[hash].js',
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(
            __dirname,
            'node_modules',
            '@descartes/d-map',
            'dist'
          ),
          to: path.resolve(__dirname, 'dist/d-map-' + dMapVersion),
          toType: 'dir',
        },
        {
          from: path.resolve(
            __dirname,
            'node_modules',
            '@descartes/d-editmap',
            'dist'
          ),
          to: path.resolve(__dirname, 'dist/d-editmap-' + dEditMapVersion),
          toType: 'dir',
        },
        {
          from: path.resolve(
            __dirname,
            'node_modules',
            '@descartes/d-inkmap',
            'dist'
          ),
          to: path.resolve(__dirname, 'dist/d-inkmap-' + dInkmapVersion),
          toType: 'dir',
        },
        {
          from: path.resolve(
            __dirname,
            'node_modules',
            '@descartes/d-inkmap',
            'dist',
            'inkmap-worker.js'
          ),
          to: path.resolve(__dirname, 'dist/inkmap-worker.js'),
        },
        {
          from: path.resolve(
            __dirname,
            'node_modules',
            '@descartes/d-geoplateforme',
            'dist'
          ),
          to: path.resolve(__dirname, 'dist/d-geoplateforme-' + dGeoplateformeVersion),
          toType: 'dir',
        },
      ],
    }),
    new HtmlWebpackPlugin({
      template: 'public/index.html',
      templateParameters: { dMapVersion, dEditMapVersion, dInkmapVersion, dGeoplateformeVersion, descartesApiPath },
    }),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    port: 4200,
    proxy: {
        //on a donc un proxy de proxy.... (pour le dev uniquement)
        '/*/proxy': {
            target: descartesServer,
            pathRewrite: {
                '/.*/proxy': '/proxy'
            }
        },
        '/*/exportPDF': {
            target: descartesServer,
            pathRewrite: {
                '/.*/exportPDF': '/exportPDF'
            }
        },
        '/*/exportPNG': {
            target: descartesServer,
            pathRewrite: {
                '/.*/exportPNG': '/exportPNG'
            }
        },
        '/*/contextManager': {
            target: descartesServer,
            pathRewrite: {
                '/.*/contextManager': '/contextManager'
            }
        },
        '/*/getFeature': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getFeature': '/getFeature'
            }
        },
        '/*/getFeatureInfo': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getFeatureInfo': '/getFeatureInfo'
            }
        },
        '/*/getImage': {
            target: descartesServer,
            pathRewrite: {
                '/.*/getImage': '/getImage'
            }
        }
    },
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', {}],
          },
        },
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader',
            options: {
              sourceMap: true,
            },
          },
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              url: false,
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
            },
          },
        ],
      },
    ],
  },
}
