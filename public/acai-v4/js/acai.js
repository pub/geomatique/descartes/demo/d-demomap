// Gestion de cookies
function createCookie(name, value, days) {
  if (days) {
    var date = new Date()
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000)
    var expires = '; expires=' + date.toGMTString()
  } else {
    var expires = ''
  }
  document.cookie =
    name + '=' + encodeURIComponent(value) + expires + '; path=/'
}

function readCookie(name) {
  var nameEQ = name + '='
  var ca = document.cookie.split(';')
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1, c.length)
    }
    if (c.indexOf(nameEQ) == 0) {
      return c.substring(nameEQ.length, c.length)
    }
  }
  return null
}

function eraseCookie(name) {
  createCookie(name, '', -1)
}

// Montrer/cacher le div de gauche
function ShowHidePanel(panelId, day) {
  var panelHidden = readCookie('CkPanelhidden')
  if (!panelHidden) {
    createCookie('CkPanelhidden', true, day)
  }
  $(panelId).toggle()
  if ($(panelId).is(':visible')) {
    $('#milieu').toggleClass('col-md-9', true)
    $('#milieu').toggleClass('col-md-12', false)
  } else {
    $('#milieu').toggleClass('col-md-9', false)
    $('#milieu').toggleClass('col-md-12', true)
  }
  if (panelHidden) {
    eraseCookie('CkPanelhidden')
  }
}

function InitShowHidePanel(panelId) {
  var panelHidden = readCookie('CkPanelhidden')
  if (!panelHidden) {
    $('#milieu').toggleClass('col-md-9', true)
    $('#milieu').toggleClass('col-md-12', false)
  } else {
    $(panelId).toggle()
    $('#milieu').toggleClass('col-md-9', false)
    $('#milieu').toggleClass('col-md-12', true)
  }
}

// Escapes special characters and returns a valid jQuery selector
function jqSelector(str) {
  return str.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1')
}

$(document).ready(function() {
  // Custom function which toggles between sticky class (is-sticky)
  var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
    var stickyHeight = sticky.outerHeight()
    var stickyTop = stickyWrapper.offset().top
    if (scrollElement.scrollTop() >= stickyTop) {
      stickyWrapper.height(stickyHeight)
      sticky.addClass('is-sticky')
    } else {
      sticky.removeClass('is-sticky')
      stickyWrapper.height('auto')
    }
  }

  // Find all data-toggle="sticky-onscroll" elements
  $('[data-toggle="sticky-onscroll"]').each(function() {
    var sticky = $(this)
    var stickyWrapper = $('<div>').addClass('sticky-wrapper') // insert hidden element to maintain actual top offset on page
    sticky.before(stickyWrapper)
    sticky.addClass('sticky')

    // Scroll & resize events
    $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
      stickyToggle(sticky, stickyWrapper, $(this))
    })

    // On page load
    stickyToggle(sticky, stickyWrapper, $(window))
  })
})
