#!/usr/bin/env node

/**
 * This will read a settings.json file based on a given environment in conf/<env>
 * or the default one in conf, replace some keys (namely project version & commit
 * hash), and write the resulting file in the backoffice app assets.
 *
 * Has to be run before building or serving the app.
 */

const path = require('path')
const { writeFile: writeFileAsync } = require('fs')
const { promisify } = require('util')
const { exec: execAsync } = require('child_process')

// promisify node async API for easier usage
const exec = promisify(execAsync)
const writefile = promisify(writeFileAsync)

const settingsFile =
  process.argv.length > 2 ? process.argv[2] : 'conf/settings.json'

let inputPath = path.join(__dirname, '..', settingsFile)
const outputPath = path.join(__dirname, '../public/settings.json')

let settingsJson = require(inputPath)

// merge object with parent if one is specified
while (settingsJson.inherits) {
  inputPath = path.join(path.dirname(inputPath), settingsJson.inherits)
  const parentJson = require(inputPath)
  delete settingsJson.inherits
  settingsJson = Object.assign(parentJson, settingsJson)
}

// reads the process stdout and remove trailing carriage return
const getStdOut = result => result.stdout.replace(/\n$/, '')

Promise.all([
  exec('npm run print-version --silent').then(getStdOut),
  exec('git rev-parse --short HEAD').then(getStdOut),
  exec('git show -s --format=%s').then(getStdOut),
])
  .then(([version, hash, message]) => {
    settingsJson.projectVersion = version
    settingsJson.commitHash = hash
    settingsJson.commitMessage = message
    return JSON.stringify(settingsJson, null, 2)
  })
  .then(settings => writefile(outputPath, settings).then(() => settings))
  .then(
    settings => {
      console.log('[INFO] Settings written:')
      console.log(settings)
    },
    err => console.log(err)
  )
