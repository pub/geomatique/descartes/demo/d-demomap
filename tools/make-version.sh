#!/usr/bin/env bash

npm version $1 --git-tag-version=false
NEWVERSION=$(npm run print-version --silent)

mvn versions:set -DnewVersion="$NEWVERSION" -DgenerateBackupPoms=false

git add .
git commit -m "chore: version $NEWVERSION"

if [[ "$NEWVERSION" =~ "SNAPSHOT" ]]; then
  echo "Skipping git tag (snapshot)"
else
  git tag "V$NEWVERSION" -f
fi
