#!/bin/sh

augtool -r /usr/share/nginx/html --noautoload --transform "Json.lns incl /settings.json" << EOF
set /files/settings.json/dict/entry[. = "descartesApiPath"]/string ${DESCARTES_API_PATH}
save
EOF

augtool -r /usr/share/nginx/html --noautoload --transform "Json.lns incl /settings.json" << EOF
set /files/settings.json/dict/entry[. = "descartesUrlRoot"]/string ${DESCARTES_URL_ROOT}
save
EOF

augtool -r /usr/share/nginx/html --noautoload --transform "Json.lns incl /settings.json" << EOF
set /files/settings.json/dict/entry[. = "georefApiPath"]/string ${GEOREF_API_PATH}
save
EOF

echo "[INFO] Settings:"
cat /usr/share/nginx/html/settings.json

exec "$@"
