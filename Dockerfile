FROM nginx:1.14.2-alpine

ENV DESCARTES_API_PATH ${DESCARTES_API_PATH:-"https://descartes-server.din.developpement-durable.gouv.fr/services/api/v1"}
ENV DESCARTES_URL_ROOT ${DESCARTES_URL_ROOT:-"https://descartes.din.developpement-durable.gouv.fr"}
ENV GEOREF_API_PATH ${GEOREF_API_PATH:-"https://georef.application.developpement-durable.gouv.fr/geoservices/api/v1"}

RUN apk add --no-cache augeas
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/nginx.conf
WORKDIR /usr/share/nginx/html
COPY public .
COPY dist .
COPY --chmod=777 ./docker-entrypoint.sh /docker-entrypoint.sh

EXPOSE 8083

ENTRYPOINT ["/docker-entrypoint.sh", "nginx", "-g", "daemon off;"]
